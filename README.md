# wgamesolv

Words Games Solver.    
Script d'aide à la résolution des jeux de mots style Wordle, SUTOM, mots-croisés, Scrabble...     
On a fait ça pour s'amuser, pas spécialement pour tricher aux jeux de mots.  
Auteurs : AntoineVe, Brunus.  
Courageux testeur des 1ères versions et précieux apporteur d'idées: DarKou.  
Licence : MIT  

Utilisation : wgamesolv.py -d dictionnaire.json -nb 7 (autres options)

-d, --dictionary : dictionnaire à utiliser  
ex : -d fr_Fc.json  
Trois dictionnaires ont été fusionnés pour n'en faire qu'un : Wordle, SUTOM et l'officiel du Scrabble  

-fl, --firstLetter : première lettre du mot  
ex : -fl C  
Cette option est intéressante que l'on connaisse ou non la première lettre, il faut en choisir une pour limiter la liste de mots possibles.  

-nb, --nbLetters : nombre de lettres qui composent le mot  
ex : -nb 7  

-ft, --firstTry : optimise le premier essai en sortant les mots de -nb lettres, commençant par -fl lettre, ne contenant aucune lettre en plusieurs occurence et contenant -ft voyelles.  
ex : -ft 4  
Cette option peut être utilisée pour le tout premier essai, pour optimiser les chances d'avoir des lettres validées.  
Cette option est ensuite inutile ou contre-productive pour les essais suivants et ne peut pas être utilisée avec la pattern.  

-nd, --noDoubleLetters : filtre les mots composé d'une seule occurence de chaque lettre.

-kl, --knownLetters : lettres déjà validées  
ex : -kl AEP

-bl, --badLetters : lettres déjà invalidées  
ex : -bl ZTOR

-p, --pattern : lettres validées et bien placées, lettres connues et mal placées, séparée par des '.' pour les lettres non validées  
Les lettres en caps sont les lettres connues et bien placées, les lettres en minuscules sont les lettres connues mais mal placées    
ex : -p L.uI... (le mots était LUCIOLE)

Exemple complet : wgamesolv.py -d fr_FR.json -nb 7 -fl L -p L.uI... -kl U -bl AMNR
