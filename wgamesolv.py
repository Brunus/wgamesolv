#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import argparse
from random import shuffle


def load_dict(dict_file):
    try:
        with open(dict_file, "r") as dico:
            liste = json.load(dico)
        return liste
    except Exception as error:
        print(f"Impossible d'ouvrir le fichier {dict_file} : {error}")
        exit(2)


def noDouble(mots):
    # Fonction d'élimination de mots contenant plusieurs occurences de lettres
    ndList = []
    for mot in mots:
        if len(list(mot)) == len(set(list(mot))):
            ndList.append(mot)
    return ndList


def firstTryFilter(mots, max_mots, pattern):
    '''
    First Try : sélectionne des mots ayant des chances de valider ou invalider
    l'une des lettres les plus utilisée en français.
    '''
    if pattern:
        return mots  # Inhibe le filtre si on utilise aussi un pattern
    else:
        bestLetters = ['E', 'A', 'I', 'R', 'S', 'N', 'T', 'O', 'L', 'U']
        bestWords = []
        shuffle(mots)
        mots = mots[:100]
        for mot in mots:
            for bestLetter in bestLetters:
                if bestLetter in mot:
                    bestWords.append(mot)
                bestWords = list(set(bestWords))
        bestWords_noDouble = noDouble(bestWords)  # Autant maximiser les chances
        if len(bestWords_noDouble) > 0:
            bestWords = bestWords_noDouble
        bestWords = bestWords[:max_mots]
        return bestWords


def resolv_fl(liste, firstletter):
    # On ne garde que les mots qui commence par FirstLetter
    firstletter = firstletter.upper()
    return [mot for mot in liste if mot[0] == firstletter]


def resolv_len(liste, lenght):
    # On ne garde que les mots qui ont la bonne longueur
    return [mot for mot in liste if len(mot) == lenght]


def resolv_pattern(liste, pattern):
    # Pattern : élimination des mots ne satisfaisant pas la pattern
    pattern_dict, not_pattern_dict = {}, {}
    for lettre in pattern:
        if lettre != "." and lettre.isupper():
            pattern_dict.update({pattern.index(lettre): lettre})
        if lettre != "." and lettre.islower():
            not_pattern_dict.update({pattern.index(lettre): lettre.upper()})
    MotsOK, MotsKO = [], []
    for mot in liste:
        for key in pattern_dict:
            if mot[key] == pattern_dict[key]:
                MotsOK.append(mot)
            else:
                MotsKO.append(mot)
        for key in not_pattern_dict:
            if mot[key] == not_pattern_dict[key]:
                MotsKO.append(mot)
    MotsKO = set(MotsKO)
    liste = [mot for mot in MotsOK if mot not in MotsKO]
    liste = list(set(liste))
    return liste


def resolv_kl(liste, lettres):
    '''
    Known Letters : élimination des mots ne comprenant pas
    les lettres validées (hors pattern)
    '''
    lettres = list(set(list(lettres.upper())))
    MotsRestants = []
    for mot in liste:
        if len(set(lettres).intersection(mot)) == len(lettres):
            MotsRestants.append(mot)
    return MotsRestants


def resolv_bl(liste, lettres):
    '''
    Bad Letters : élimination des mots contenant
    des lettres invalidées (hors pattern)
    '''
    lettres = list(set(list(lettres.upper())))
    MotsSuppr = []
    for mot in liste:
        for lettre in lettres:
            if lettre in mot:
                MotsSuppr.append(mot)
    return [mot for mot in liste if mot not in MotsSuppr]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Cheating with SUTOM'
    )
    parser.add_argument(
        '-d',
        '--dictionary',
        help='Dictionnaire des mots du jeu',
        required=True
    )
    parser.add_argument(
        '-fl',
        '--firstLetter',
        help='Première lettre',
        default=False
    )
    parser.add_argument(
        '-nb',
        '--nbLetters',
        type=int,
        help='Nombre de lettres, un entier',
        default=False,
        required=True
    )
    parser.add_argument(
        '-kl',
        '--knownLetters',
        default=False,
        help='Lettres connues, hors première, sans espaces : -kl BE'
    )
    parser.add_argument(
        '-bl',
        '--badLetters',
        default=False,
        help='Lettres non valides, sans espaces : -bl AKL'
    )
    parser.add_argument(
        '-p',
        '--pattern',
        default=False,
        help='Placement : bien placée en majuscule, mal placée en minuscule et non existante par un \'.\'. E.g. : Ra..e.T'
    )
    parser.add_argument(
        '-nd',
        '--noDoubleLetters',
        help="Option pour ne chercher que des mots ne contenant qu'une occurence de chaque lettre",
        default=False,
        action="store_true"
    )
    parser.add_argument(
        '-ft',
        '--firstTry',
        default=False,
        type=int,
        help="Sélectionne des mots commençants par firstLetter et composés de ft voyelles différentes: -ft 4",
    )

    args = parser.parse_args()

    if args.pattern and args.nbLetters and len(args.pattern) != args.nbLetters:
        print("Tu as merdé ta pattern petit scarabé !")
        exit(2)

    liste = load_dict(args.dictionary)
    liste = resolv_len(liste, args.nbLetters)
    if args.firstLetter:
        liste = resolv_fl(liste, args.firstLetter)
    if args.pattern:
        liste = resolv_pattern(liste, args.pattern)
    if args.noDoubleLetters:
        liste = noDouble(liste)
    if args.knownLetters:
        liste = resolv_kl(liste, args.knownLetters)
    if args.badLetters:
        liste = resolv_bl(liste, args.badLetters)
    if args.firstTry:
        liste = firstTryFilter(liste, args.firstTry, args.pattern)
        if len(liste) == 1:
            print(f"Essaie : \"{liste[0]}\"")
        elif len(liste) > 1:
            print(f"Mots à tester : {liste}")
        else:
            print("Hmmm... Je crois qu'on s'est perdu...")
        exit(0)
    if len(liste) > 1:
        print(liste)
        print(f"Il y a {len(liste)} mots dans la liste")
    elif len(liste) == 1:
        print(f"La solution est \"{liste[0]}\".")
    else:
        print("Hmmm... Je crois qu'on s'est perdu...")
